//IMPORTS
import React from "react";
import "./resources/scss/App.scss";
import Header from "../Header/Header";
import Footer from "../Footer/Footer";
import Calculator from "../Calculator/Calculator";

/**
 *
 * @constructor
 */
export default function App():JSX.Element{
    //Constants
    const TITLE = "Calculator";
    const AUTHOR = "Ángel Herce Soto";

    return (
        <React.Fragment>
            <Header title={TITLE}/>

            <main id="main-content">
                <div className="wrapper">
                    <section id="calculator">
                        <Calculator/>
                    </section>
                </div>
            </main>

            <Footer author={AUTHOR}/>
        </React.Fragment>
    );
}