//IMPORTS
import React from "react";
import "./resources/scss/Button.scss";

/**
 *
 * @param props
 * @constructor
 */
export default function Button( props:any ):JSX.Element{
    //Constants
    const NAME = props.name;
    const TITLE = props.title;
    const CLICK = props.click;

    return (
      <button data-name={NAME} className={`btn ${NAME}`} onClick={CLICK}>
          <span className="text">{TITLE}</span>
      </button>
    );
}