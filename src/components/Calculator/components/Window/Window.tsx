//IMPORTS
import React from "react";
import "./resources/scss/Window.scss";

/**
 * Window
 * @param props
 * @constructor
 */
export default function Window( props:any ):JSX.Element{
    const DATA = props.data;

    return (
        <div className="window">
            <div className="wrapper">
                <span className="data">{DATA}</span>
            </div>
        </div>
    );
}