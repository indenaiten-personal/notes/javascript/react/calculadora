//IMPORTS
import React from "react";
import "./resources/scss/Calculator.scss";
import Window from "./components/Window/Window";
import Button from "./components/Button/Button";
import ActionService from "./service/SimpleCalcService/ActionService";
import SumActionService from "./service/SimpleCalcService/impl/SumActionService";
import DiffActionService from "./service/SimpleCalcService/impl/DiffActionService";
import MultiplicationActionService from "./service/SimpleCalcService/impl/MultiplicationActionService";
import DivisionActionService from "./service/SimpleCalcService/impl/DivisionActionService";
import CalculatorService from "./service/CalculatorService.ts/CalculatorService";
import CalculatorServiceImpl from "./service/CalculatorService.ts/impl/CalculatorServiceImpl";

/**
 *
 * @constructor
 */
export default function Calculator():JSX.Element{
    const calculatorService:CalculatorService = new CalculatorServiceImpl();

    //componentDidMount
    React.useEffect( () => {
        const LOGIC = ( event ) => {
            const KEY = event.which || event.keyCode;

            switch( KEY ){
                case 13: calculatorService.equal( event ); console.log( `KEY: =`); break;
                case 27: calculatorService.clear( event ); console.log( `KEY: ESC`); break;
                case 8: calculatorService.remove( event ); console.log( `KEY: retroceso`); break;
                case 46: calculatorService.clear( event ); console.log( `KEY: supr`); break;
                case 42: case 106: calculatorService.setActionService( event, new MultiplicationActionService() ); console.log( `KEY: *`); break;
                case 43: case 107: calculatorService.setActionService( event, new SumActionService() ); console.log( `KEY: +`); break;
                case 44: case 188: case 110: calculatorService.setValue( event, "," ); console.log( `KEY: ,`); break; //COMA
                case 45: case 109: calculatorService.setActionService( event, new DiffActionService() ); console.log( `KEY: -`); break;
                case 46: case 190: calculatorService.setValue( event, "," ); console.log( `KEY: ,`); break; //PUNTO
                case 47: case 111: calculatorService.setActionService( event, new DivisionActionService() ); console.log( `KEY: /`); break;
                case 48: case 96: calculatorService.setValue( event, "0" ); console.log( `KEY: 0`); break;
                case 49: case 97: calculatorService.setValue( event, "1" ); console.log( `KEY: 1`); break;
                case 50: case 98: calculatorService.setValue( event, "2" ); console.log( `KEY: 2`); break;
                case 51: case 99: calculatorService.setValue( event, "3" ); console.log( `KEY: 3`); break;
                case 52: case 100: calculatorService.setValue( event, "4" ); console.log( `KEY: 4`); break;
                case 53: case 101: calculatorService.setValue( event, "5" ); console.log( `KEY: 5`); break;
                case 54: case 102: calculatorService.setValue( event, "6" ); console.log( `KEY: 6`); break;
                case 55: case 103: calculatorService.setValue( event, "7" ); console.log( `KEY: 7`); break;
                case 56: case 104: calculatorService.setValue( event, "8" ); console.log( `KEY: 8`); break;
                case 57: case 105: calculatorService.setValue( event, "9" ); console.log( `KEY: 9`); break;
            }
        }

        window.addEventListener( "keydown", LOGIC );
        return () => window.removeEventListener( "keydown", LOGIC ); //componentWillUnmount
    }, [calculatorService]);

    return (
      <div className="calculator">
          <div className="wrapper">
              <header>
                  <div className="wrapper">
                    <Window data={calculatorService.getPrompt()}/>
                  </div>
              </header>

              <main>
                  <div className="wrapper">
                      <div className="buttons">
                          <Button name="number-1" title="1" click={event => calculatorService.setValue( event, "1" )}/>
                          <Button name="number-2" title="2" click={event => calculatorService.setValue( event, "2" )}/>
                          <Button name="number-3" title="3" click={event => calculatorService.setValue( event, "3" )}/>
                          <Button name="number-4" title="4" click={event => calculatorService.setValue( event, "4" )}/>
                          <Button name="number-5" title="5" click={event => calculatorService.setValue( event, "5" )}/>
                          <Button name="number-6" title="6" click={event => calculatorService.setValue( event, "6" )}/>
                          <Button name="number-7" title="7" click={event => calculatorService.setValue( event, "7" )}/>
                          <Button name="number-8" title="8" click={event => calculatorService.setValue( event, "8" )}/>
                          <Button name="number-9" title="9" click={event => calculatorService.setValue( event, "9" )}/>
                          <Button name="number-clear" title="C" click={event => calculatorService.clear(event)}/>
                          <Button name="number-0" title="0" click={event => calculatorService.setValue( event, "0" )}/>
                          <Button name="number-comma" title="," click={event => calculatorService.setValue( event, "," )}/>
                          <Button name="number-sum" title="+" click={event => calculatorService.setActionService( event, new SumActionService() )}/>
                          <Button name="number-diff" title="-" click={event => calculatorService.setActionService( event, new DiffActionService() )}/>
                          <Button name="number-multiplication" title="x" click={event => calculatorService.setActionService( event, new MultiplicationActionService() )}/>
                          <Button name="number-division" title="/" click={event => calculatorService.setActionService( event, new DivisionActionService() )}/>
                          <Button name="number-remove" title="DEL" click={event => calculatorService.remove( event )}/>
                          <Button name="number-equal" title="=" click={event => calculatorService.equal( event )}/>
                      </div>
                  </div>
              </main>
          </div>
      </div>
    );
}