//IMPORTS
import ActionService from "../ActionService";

/**
 *
 */
export default class DiffActionService implements ActionService{
    /** */
    private readonly SYMBOL:string = "-";

    /**
     *
     * @param data
     */
    exec(data:string):number {
        if( data.length > 0 && data.includes( this.getSymbol() ) ) {
            data = data.replace( /,/g, "." );
            const [n1, n2] = data.split( this.getSymbol() );
            return Number.parseFloat(`0${n1}`) - Number.parseFloat(`0${n2}`);
        }
        return null;
    }

    /**
     *
     */
    getSymbol():string {
        return this.SYMBOL;
    }
}