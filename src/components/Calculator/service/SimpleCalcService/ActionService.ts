/**
 *
 */
export default interface ActionService {
    /**
     *
     * @param data
     */
    exec( data:string ):number;

    /**
     *
     */
    getSymbol():string;
}