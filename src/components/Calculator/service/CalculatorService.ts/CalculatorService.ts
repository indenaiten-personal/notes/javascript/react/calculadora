import ActionService from "../SimpleCalcService/ActionService";

export default interface CalculatorService{
    getPrompt():string;
    setActionService( event:any, action:ActionService ):void;
    clear(event:any ):void;
    remove(event:any ):void;
    setValue( event:any, value:string ):void;
    equal( event:any ):void;
}