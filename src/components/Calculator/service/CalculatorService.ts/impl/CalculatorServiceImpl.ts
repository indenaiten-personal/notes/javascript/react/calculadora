import CalculatorService from "../CalculatorService";
import ActionService from "../../SimpleCalcService/ActionService";
import React from "react";

export default class CalculatorServiceImpl implements CalculatorService{
    private promptState:string = null;
    private actionState:ActionService = null;
    private newResultState:boolean = null;
    private promptStateUpdate:Function = null;
    private actionStateUpdate:Function = null;
    private newResultStateUpdate:Function = null;

    // constructor( promptState:React.ComponentState, actionState:React.ComponentState, newResultState:React.ComponentState ){
    constructor(){
        const promptState = React.useState("");
        const actionState = React.useState(null );
        const newResultState = React.useState(true);
        this.promptState = promptState[0];
        this.promptStateUpdate = promptState[1];
        this.actionState = actionState[0];
        this.actionStateUpdate = actionState[1];
        this.newResultState = newResultState[0];
        this.newResultStateUpdate = newResultState[1];

    }

    getPrompt(): string {
        return this.promptState;
    }

    equal(event: any): void {
        if( this.actionState != null ) {
            const result:number = this.actionState.exec( this.promptState );
            this.actionStateUpdate( null );
            if( result != null ) {
                this.promptStateUpdate(result.toString());
            }
        }
    }

    clear(event: any): void {
        this.promptStateUpdate( "" );
        this.actionStateUpdate( null );
        this.newResultStateUpdate( true );
    }

    remove(event: any): void {
        const PROMPT_LENGTH = this.promptState.length;

        if( PROMPT_LENGTH > 1 ) {
            const NEW_PROMPT = this.promptState.substring(0, this.promptState.length - 1);
            this.promptStateUpdate( NEW_PROMPT );

            if( !NEW_PROMPT.match( /[\+\-\*\/]/g ) ) {
                this.actionStateUpdate(null);
            }
        }
        else if( PROMPT_LENGTH == 1 ){
            this.clear( event );
        }
    }

    setActionService(event: any, action: ActionService): void {
        if( this.actionState == null ) {
            this.promptStateUpdate( this.promptState + action.getSymbol());
            this.actionStateUpdate( action );
        }
    }

    setValue(event: any, value:string): void {
        if( this.newResultState ){
            this.promptStateUpdate( value );
            this.actionStateUpdate( null );
            this.newResultStateUpdate( false );
        }
        else {
            this.promptStateUpdate( this.promptState + value );
        }
    }

}