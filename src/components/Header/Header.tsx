//IMPORTS
import React from "react";
import "./resources/scss/Header.scss";

/**
 * Header
 * @param props
 * @constructor
 */
export default function Header( props:any ):JSX.Element{
    //Constants
    const TITLE:string = props.title;

    return (
      <header id="main-header">
          <div className="wrapper">
              <h1>{TITLE}</h1>
          </div>
      </header>
    );
}