//IMPORTS
import React from "react";
import "./resources/scss/Footer.scss";

/**
 * Footer
 * @param props
 * @constructor
 */
export default function Footer( props:any ):JSX.Element{
    //Constants
    const AUTHOR = props.author;

    return (
      <footer id="main-footer">
          <div className="wrapper">
              <small>{AUTHOR}</small>
          </div>
      </footer>
    );
}