//IMPORTS
import React from "react";
import ReactDOM from "react-dom/client";
import "./resources/scss/index.scss"
import App from "./components/App/App";

const ROOT:ReactDOM.Root = ReactDOM.createRoot( document.getElementById( "root" ));
ROOT.render(
    <React.StrictMode>
        <App/>
    </React.StrictMode>
);